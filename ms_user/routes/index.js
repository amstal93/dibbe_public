var express = require('express');
var router = express.Router();
var config = require('config');
var bcrypt = require('bcryptjs');
//mysql dbconnection
//Getconfig
var mysql = require('mysql');
var con = mysql.createConnection({
  host: config.get('mysql.host'),
  user: config.get('mysql.user'),
  password: config.get('mysql.password'),
  database: config.get('mysql.database'),
  port: config.get('mysql.port')
});
//neuen User anlegen
router.post('/', function(req, res, next) {
  bcrypt.hash(req.body.password, 10, function(err, hash){
    var sql = "INSERT INTO user (email, pw, user_name) VALUES ('"+req.body.email+"', '"+hash+"', '"+req.body.email+"')";
    con.query(sql, function (err, result){
      if (err) res.send("fail");
      else res.send("OK");
    })
  });
});

//User login
router.post('/login', function(req, res, next) {
  var sql = "SELECT email, pw, user_name, rang, image FROM user WHERE email='"+req.body.email+"'";
  con.query(sql, function (err, result, fields){
    if (err) res.send(err);
    if (!result[0]) res.send("fail");
    else {
      bcrypt.compare(req.body.password, result[0].pw, function(err, resu) {
        if (resu) res.send(result[0]);
        else res.send("fail");
      });
    };
  });
});

//google login,
//Erstelle Account wenn noch nicht vorhanden
router.post('/google', function(req, res, next) {
  var data = {
    "email": req.body.emails[0].value,
    "user_name" : req.body.nickname || req.body.displayName,
    "rang" : "user",
    "image": req.body._json.image.url
  };
  console.log(data);
  var sql = "SELECT email, user_name, rang, googleid FROM user WHERE googleid='"+req.body.id+"' AND email='"+data.email+"'";
  con.query(sql, function (err, result, fields){
    if (err) res.send(err);
    if (!result[0]) {
      //noch kein Account vorhanden, erstelle neuen
      var insert_sql = "INSERT INTO user (email, user_name, rang, googleid, image) VALUES ('"+req.body.emails[0].value+"', '"+data.user_name+"', '"+data.rang+"', '"+req.body.id+"', '"+data.image+"')";
      con.query(insert_sql, function (error, resu){
        if (error) res.send("fail");
        else {
          console.log("Neuer Accout wurde erfolgreich erstellt!");
          res.send(data);
        }
      });
    } else {
      //Account bereits vorhanden
      console.log("Google-Account bereits gespeichert, login erfolgreich!");
      res.send(data);
    };
  });
});

//User aktualisieren, identifizierung via email
router.patch('/profil/:mail', function(req, res, next) {
  var sql = "UPDATE user SET "
  if (req.body.image){
    sql = sql + "image='"+req.body.image+"'";
  }
  sql = sql + " WHERE email='"+req.params.mail+"'";
  con.query(sql, function (err, result, fields){
    if (err) res.send(err);
    else res.send("OK");
  });
});

module.exports = router;
