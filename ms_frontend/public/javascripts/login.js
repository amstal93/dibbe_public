$(document).ready(function(){
    if (filter_is_in("login", "false")){
        //fehlerhafter login
        //schriftfarbe auf rot setzen und rahmen auf rot
        $("#login_page .txt_input").css("border", "1px red solid");
        $("#error").css("color", "red");
    };
    if (filter_is_in("logout", "true")){
        $("#error").css("color", "green");
    };
    if (filter_is_in("reg_page", "true")){
      //Direkter Aufruf der Registration
      $("#login_page").css('display', 'none');
      $("#reg_page").css('display', 'block');
    }
    if (filter_is_in("reg", "false")){
      //Fehler bei der Registration
      $("#reg_page .txt_input").css("border", "1px red solid");
      $("#reg_error").css("color", "red");
    }
    if (filter_is_in("google", "false")){
        //fehlerhafter login mit google
        $("#error").css("color", "red");
    };
});

function bt_reg(){
    $("#login_page").css("animation-name", "fadeout");
    setTimeout(function() {
        $("#login_page").css('display', 'none');
        $("#reg_page").css('display', 'block');
        $("#reg_error").html("");
        history.pushState(null, null, window.location.pathname+filter_add("reg_page", "true", true));
    }, 500);
}

function bt_back_login(){
  $("#reg_page").css("animation-name", "fadeout");
  setTimeout(function() {
      $("#reg_page").css('display', 'none');
      $("#login_page").css('display', 'block');
      $("#error").html("");
      history.pushState(null, null, window.location.pathname);
  }, 500);
}
//validierung der passwörter
$("#pw_rep").keyup(function() {
  if ($("#pw_rep").val() != $("#pw").val()) {
    $("#pw_rep").css("border", "3px red solid");
  } else {
    $("#pw_rep").css("border", "0px");
    $('#reg_page input[type="submit"]').removeAttr('disabled');
  }
});
