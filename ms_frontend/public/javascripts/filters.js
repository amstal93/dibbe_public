// Filter funktionen
function filter_add(query, value, reset=false){
    if (reset===true){
        return "?"+query+"="+value;
    } else {
        if (window.location.search != ""){
            if (filter_is_in(query, value)){
                return window.location.search;
            }
            return window.location.search+"&"+query+"="+value;
        } else {
            return "?"+query+"="+value;
        }
    } 
}
//Funktion zur prüfung ob eine query value paar schon im searchstring ist
function filter_is_in(query, value){
    if (window.location.search.indexOf(query+"="+value) != -1){
        return true;
    }
    return false;
}    
