//safe input values
var old_values = {};
$(document).ready(function(){
  if (filter_is_in("edit", "true")){
    old_values["email"] =  $('#email input').val();
    old_values["name"] =  $('#name input').val();
  };
});

function show_save(id){
  if ($("#"+id+" input").val() != old_values[id]){
      $("#"+id+" button").css("display", "block");
  } else {
      $("#"+id+" button").css("display", "none");
  }
}

//Funktion zur aktualisierung der Bilder-Vorschau
function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      document.getElementById('profile_bild').src=e.target.result;
      $("#save_img").css("display", "block");
    }

    reader.readAsDataURL(input.files[0]);
  }
}

//FUnktion zum speichern eines Bildes
function save_img(){
    var file = document.getElementById("image").files[0];
    if (file){
      var myFormData = new FormData();
      myFormData.append('image', file);
      //ladebalken einblenden
      document.querySelector(".bar").style.display = "block";
      $.ajax({
          type:'POST',
          url: '/api/profile_img',
          complete: function (response) {
            if (response.responseText != "fail"){
              document.getElementById("img_err").innerHTML = "Bilder erfolgreich hochgeladen!";
              //Profilbild in der menubar aktualisieren
              document.getElementById("profilbild").src = response.responseText;
            } else {
              document.getElementById("img_err").innerHTML = "Fehler beim hochladen des Bildes!";
            }
            //ladebalken ausblenden
            document.querySelector(".bar").style.display = "none";
            },
            processData: false, // important
            contentType: false, // important
            dataType : 'json',
            data: myFormData
        });
    } else {
      document.getElementById("img_err").innerHTML = "Fehler beim hochladen des Bildes!";
    }
}
