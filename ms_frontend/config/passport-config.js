const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
var config = require("config");
var googlecnf = config.get('google');
var request = require('request-promise');
var options = {
    json: true,
    insecure: true,
    rejectUnauthorized: false,
    method: "POST"
};

passport.serializeUser((user, done) => {
    done(null, user);
});

passport.use(
    new GoogleStrategy({
        // options for google strategy
        clientID: googlecnf.clientID,
        clientSecret: googlecnf.clientSecret,
        callbackURL: googlecnf.callbackURL
    }, (accessToken, refreshToken, profile, done) => {
        // passport callback function
        options.uri = config.get("msluser")+"/google";
        options.body = profile;
        request(options).then(data => {
              if (data != "fail"){
                console.log("OK: Login mit Google hat geklappt!");
                done(null, data);
              } else {
                console.log("ERROR: Der Login hat irgendwie net geklappt!");
                done(null, data);
              }
        }).catch(err => {
              console.log("ERROR: Fehler beim Verbinden mit ms_user!");
              done(null, err);
        });
    })
);
