var express = require('express');
var router = express.Router();
var request = require('request-promise');
var config = require('config');
var user_data = require('../../lib/user_data');

//Getconfig
var backendip = config.get('ip');
var main_uri = backendip+'/lebensmittel';
var options = {
    json: true,
    insecure: true,
    rejectUnauthorized: false
};

router.get('*', function(req, res, next) {
  /*  if (req.session.rang != "admin"){
        res.render('error', {error : "Nice try, #keinadmin"});
    }; */
    //leeres data_render
    req.data_render = {};
    if (req.query.data === 'new') {
        //Nur neue Produkte laden
        options.uri = main_uri + "?data=new";
    } else {
        //Gesamte Produkt-Liste laden
        options.uri = main_uri;
    }
    options.method = 'GET';
    console.log(options.uri);
    req.data_render.user_data = user_data.get_user_data(req);
    req.data_render.pagetitle = "Produktdatenbank";
    request(options).then(data => {
          console.log("OK: msldata ausgelesen.");
          req.data_render.message = "Produkt-Liste wurde erfolgreich ausgelesen!";
          req.data_render.produkt_list = data;
          next();
    }).catch(err => {
          console.log("ERROR: msldata ausgelesen.");
          req.data_render.message = "Fehler beim Laden der Produkt-Liste!";
          req.data_render.produkt_list_error = err;
          next();
    });
});

router.get('/', function(req, res, next) {
    res.render("admin/admin_data", req.data_render);
});

//Produkt-Anzeige
router.get('/:produkt_id', function(req, res, next) {
    //uri setzen fuer Backend
    options.uri = main_uri + '/' + req.params.produkt_id;
    options.method = 'GET';
    console.log(options.uri);
    request(options).then(produkt => {
          console.log("OK: Produkt: "+req.params.produkt_id+" ausgelesen.");
          console.log(produkt);
          req.data_render.produkt = produkt;
          res.render('admin/admin_data', req.data_render);
    }).catch(err => {
          console.log("ERROR: Fehler beim Auslesen des Produkts: "+req.params.produkt_id);
          req.data_render.produkt_error = err;
          res.render('admin/admin_data', req.data_render);
    });
});





module.exports = router;
