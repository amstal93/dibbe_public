var express = require('express');
var router = express.Router();
var request = require('request-promise');
var user_data = require('../../lib/user_data');

router.get('/', function(req, res, next) {
    res.render('admin/admin_scan', {title : "Produktscan", user_data : user_data.get_user_data(req)}); 
});


module.exports = router;