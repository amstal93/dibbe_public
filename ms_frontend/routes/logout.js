var express = require('express');
var router = express.Router();
var request = require('request-promise');
var config = require('config');

router.get('/', function(req, res, next) {
    req.session.loggin = false;
    res.redirect("/login?logout=true");
    
});

module.exports = router;