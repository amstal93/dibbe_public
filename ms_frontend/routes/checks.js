var express = require('express');
var router = express.Router();
var request = require('request-promise');
var config = require('config');


//check_Session

router.get('/', function(req, res, next) {
   if (req.session.loggin){
       next();
   } else {
       res.redirect('/login');
   }
});

module.exports = router;