var express = require('express');
var router = express.Router();
var passport = require('passport');

// auth with google+
router.get('/google', passport.authenticate('google', {scope: ['profile', 'email']}));

// callback route for google to redirect to
// hand control to passport to use code to grab profile info

router.get('/google/redirect',
  passport.authenticate('google', { failureRedirect: '/login?google=false' }),
  function(req, res) {
    if (req.user) {
      if (req.user != "fail"){
        req.session.loggin = true;
        req.session.data = req.user;
        res.redirect('/');
      } else {
          res.redirect('/login?google=false');
      }
    } else res.redirect('/login?google=false');
});

module.exports = router;
