var express = require('express');
var router = express.Router();
var user_data = require('../lib/user_data');

router.get('/', function(req, res, next) {
    if (req.query.edit == "true"){
      res.render('profile', {pagetitle : "Profil bearbeiten", edit : true, user_data : user_data.get_user_data(req)});
    } else {
      res.render('profile', {pagetitle : "Profil", show : true, user_data : user_data.get_user_data(req)});
    }
});

module.exports = router;
