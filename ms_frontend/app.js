var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var helmet = require('helmet');
var exphbs = require('express-handlebars')
var passport = require('passport');
var passportSetup = require('./config/passport-config');
var config = require('config');
//Session
var redis = require('redis');
var session = require('express-session');
var redisStore = require('connect-redis')(session);
var redis_cnf = config.get('redis');
var client = redis.createClient(redis_cnf.port, redis_cnf.host, {no_ready_check: true});
client.auth(redis_cnf.passwort, function (err) {
    console.log(err);
});
client.on('connect', function() {
    console.log('Connected to Redis');
});

var index = require('./routes/index');
var login = require('./routes/login');
var logout = require('./routes/logout');
var checks = require('./routes/checks');
var api = require('./routes/api');
var admin_data = require('./routes/admin/admin_data');
var admin_scan = require('./routes/admin/admin_scan');
var profile = require('./routes/profile');
var auth = require('./routes/auth');

var app = express();

app.use(passport.initialize());
app.use(passport.session());

app.use(session({
  secret: 'kartoffelsalat',
  store : new redisStore({client: client,
                          ttl: 260}),
  resave: false,
  saveUninitialized: true
}))

// view engine setup
app.engine('handlebars', exphbs({defaultLayout: 'layout'}));
app.set('view engine', 'handlebars');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json({ type: 'application/*+json' }))
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/login', login);
app.use('/auth', auth);
//alles ab Stern nicht mehr ohne account
app.use('*', checks);
app.use('/', index);
app.use('/logout', logout);
app.use('/api', api);
app.use('/admin/data', admin_data);
app.use('/admin/scan', admin_scan);
app.use('/profile', profile);



// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
