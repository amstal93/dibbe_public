CREATE DATABASE dibbe;
USE dibbe;
CREATE TABLE user (
        id int NOT NULL AUTO_INCREMENT UNIQUE,
        email varchar(255) UNIQUE,
        pw varchar(255),
        user_name varchar(255),
        rang varchar(255),
        googleid varchar(255),
        image varchar(255) DEFAULT "/images/no_pic.jpg",
        PRIMARY KEY (ID)
);
INSERT INTO user (email, pw, user_name, rang)
       VALUES ("user@dibbe.com",
               "$2a$10$9SstMu.Z1azc1Tm5BAQvV.7idOVnE63aNSahVbqHqLWNUbgNMpvDq",
               "Test User",
               "user");
INSERT INTO user (email, pw, user_name, rang)
       VALUES ("admin@dibbe.com",
               "$2a$10$9SstMu.Z1azc1Tm5BAQvV.7idOVnE63aNSahVbqHqLWNUbgNMpvDq",
                "Admin User",
                "admin");
