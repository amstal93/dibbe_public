var express = require('express');
var router = express.Router();
var Lebensmittel = require('../models/lebensmittel');


/* GET ALLE LEBENSMITTEL*/
/* POST NEUES LEBENSMITTEL */
router.post('/', function(req, res, next) {
    console.log("POST /Lebensmittel");
    console.log(req.body);
    Lebensmittel.create(req.body)
      .then(function(result){
        res.status(200).send("OK");
        console.log("OK: Lebensmittel erfolgreich gespeichert.");
      })
      .catch(function(err){
        console.log("ERROR: Fehler beim Speichern eines Lebensmittel.");
        console.log(err);
        res.status(400).send(err);  
      })
});

/* GET ALLE LEBENSMITTEL*/
router.get('/', function(req, res, next) {
    console.log("GET /Lebensmittel");
    var find_items = {};
    if (req.query.data === "new"){
        find_items = {};
    }
    Lebensmittel.find(find_items).select('_id barcode name')
        .then(function(result){
        res.status(200).send(result);
        console.log("OK: Lebensmittel erfolgreich geladen.");
        })
    .catch(function(err){
        res.status(400).send(err);
        console.log("ERROR: Fehler beim Laden der Lebensmittel.");
    });
});

/* GET BESTIMMTES LEBENSMITTEL */
router.get('/:produktid', function(req, res, next) {
    console.log("GET /Lebensmittel/"+req.params.produktid);
    Lebensmittel.findOne({'_id' : req.params.produktid})
        .then(function(result){
        res.status(200).send(result);
        console.log("OK: Lebensmittel erfolgreich ausgelegesen");
        })
    .catch(function(err){
        res.status(400).send(err);
        console.log("ERROR: Fehler beim auslesen des Lebensmittels"+req.params.produktid);
    });
});

/* PUT UPDATE VORHANDENES LEBENSMITTEL */
router.patch('/:produktid', function(req, res, next) {
    console.log("PATCH /Lebensmittel/"+req.params.produktid);
    console.log(req.body);
    Lebensmittel.findByIdAndUpdate(req.params.produktid, req.body)
        .then(function(result){
        console.log(result);
        res.status(200).send("OK");
        console.log("OK: Lebensmittel wurde erfolgreich gepatched.");
        })
    .catch(function(err){
        res.status(400).send(err);
        console.log("ERROR: Fehler patchen des Lebensmittel.");
    });
});
    
module.exports = router;
