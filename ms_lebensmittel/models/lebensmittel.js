var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/* DATENBANK SCHEMA LEBENSMITTEL */

/* BARCODES */
var barcode = {
    code: {
        type: String,
        lowercase: true,
        required: true,
        unique: true
    },
    typ: {
        type: String,
        required: true
    }
}

/* NAEHRWERTE */
var naehrwerte = {
    kcal : {
        type : String,
        default : 0
    },
    eiweiß : {
        type : String,
        default : 0
    },
    kohlenhydrate : {
        k_anzahl : {
            type : String,
            default : 0
        },
        zucker : {
            type : String,
            default : 0
        }
    },
    fett : {
        f_anzahl : {
            type : String,
            default : 0
        },
        gesaetiigt : {
            type : String,
            default : 0
        }
    },
    ballaststoffe : {
        type : String,
        default : 0
    },
    natrium : {
        type : String,
        default : 0
    },
    salz : {
        type : String,
        default : 0
    }
}

/* Zusammengesetztes Schema */
var LebensmittelSchema = new Schema({
    name: {
        type: String,
        unique: true,
        required: true
    },
    barcode: barcode,
    naehrwerte : naehrwerte,
    masseinheit : {
        gewicht : {
            type : String,
            default : 0
            },
        m_anzahl  : {
            type : String,
            default : 0
        }
    },
    verderblich : {
        type : Boolean,
        default : false
    },
    erstellt : {
        type : Date,
        default : Date.now
    },
    image : {
        type : String,
        default : "/images/no_pic.jpg"
    }
        
});

var Lebensmittel = mongoose.model('lebensmittel', LebensmittelSchema);

module.exports = Lebensmittel;